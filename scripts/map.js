dojo.require("esri.map");
dojo.require("esri.layers.WebTiledLayer");

var map, selectedDate, tileInfo, nasaSubDomains;

function init() {
  var ext = esri.geometry.Extent;
  var bounds = new ext({"xmin":-12431294,"ymin":10604908,"xmax":-3038712,"ymax":15007681,"spatialReference":{"wkid":4326}});
  map = new esri.Map("map", {
    extent: bounds,
    wrapAround180: false
  });
  dojo.connect(map, "onLoad", function(map) {
    dojo.connect(dijit.byId("map"), "resize", map, map.resize);
  });

  //tile info is optional, but is good to have so the user doesn't zoom wayyyyyy out.
  tileInfo = new esri.layers.TileInfo({
    "rows" : 256,
    "cols" : 256,
    "dpi" : 96,
    "format" : "JPEG",
    "compressionQuality" : 0,
    "origin" : { "x" : -20037508.340000, "y" : 20037508.340000 },
    "spatialReference" : { "wkid" : 4326 },
    "lods" : [
      {"level" : 0, "resolution" : 156543.033928, "scale" : 591657528},
      {"level" : 1, "resolution" : 78271.51695000000472646207, "scale" : 295828764},
      {"level" : 2, "resolution" : 39135.75847500000236323103, "scale" : 147914382},
      {"level" : 3, "resolution" : 19567.87923750000118161552, "scale" : 73957191},
      {"level" : 4, "resolution" : 9783.93961875000059080776, "scale" : 36978595},
      {"level" : 5, "resolution" : 4891.96980937500029540388, "scale" : 18489298},
      {"level" : 6, "resolution" : 2445.98490468750014770194, "scale" : 9244649},
      {"level" : 7, "resolution" : 1222.99245234375007385097, "scale" : 4622324},
      {"level" : 8, "resolution" : 611.49622617187503692548, "scale" : 2311162}
     ]
  });

  //sets current date
  selectedDate = formatDate(new Date());

  //these are the sub domains associated with http://vis.earthdata.nasa.gov
  nasaSubDomains = ["map1a", "map1b", "map1c"];

  //initial baselayer
  var modisBaseLayer = new esri.layers.WebTiledLayer("http://${subDomain}.vis.earthdata.nasa.gov/wmts-geo/wmts.cgi?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=MODIS_Terra_CorrectedReflectance_TrueColor&STYLE=&TILEMATRIXSET=EPSG4326_250m&TILEMATRIX=${level}&TILEROW=${row}&TILECOL=${col}&FORMAT=image%2Fjpeg", {
    "subDomains": nasaSubDomains, 
    "copyright": "NASA",
    "id": "modis-baselayer",
    "tileInfo": tileInfo
  });
  map.addLayer(modisBaseLayer);

  //initial overlay... hidden by default
  var modisOverlay = new esri.layers.WebTiledLayer("http://${subDomain}.vis.earthdata.nasa.gov/wmts-geo/wmts.cgi?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=MODIS_Terra_Snow_Cover&STYLE=&TILEMATRIXSET=EPSG4326_1km&TILEMATRIX=${level}&TILEROW=${row}&TILECOL=${col}&FORMAT=image%2Fpng", {
    "subDomains": nasaSubDomains, 
    "copyright": "NASA",
    "id": "modis-overlay",
    "visible": false,
    "tileInfo": tileInfo
  });
  map.addLayer(modisOverlay);

//country and water boundaries overlay
var boundaries = new esri.layers.WebTiledLayer("http://${subDomain}.vis.earthdata.nasa.gov/wmts-geo/wmts.cgi?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=sedac_bound&STYLE=&TILEMATRIXSET=EPSG4326_250m&TILEMATRIX=${level}&TILEROW=${row}&TILECOL=${col}&FORMAT=image%2Fpng", {
    "subDomains": nasaSubDomains, 
    "copyright": "NASA",
    "id": "boundaries",
    "tileInfo": tileInfo
  });
  map.addLayer(boundaries);
}

//resize map
function resizeMap() {
  var center = map.extent.getCenter();
  map.resize();
  //to handle the splitter minimizing the left panel
  setTimeout(function(){ map.centerAt(center); },300);
  
}

function changeBaseLayer(product) {
  if(map) {
    if(product == "") { //user is turning off baselayer
      map.getLayer("modis-baselayer").hide();
      return; //FUNCTION ENDS HERE.
    }

    //user has selected another baselayer
    //remove existing baselayer
    var lyr = map.getLayer("modis-baselayer");
    map.removeLayer(lyr);

    //the data in the listbox (of all the baselayers) contains a value field. 
    //the value is a combination of the resolution and the product name, separated by a "|")
    //formatProduct returns an array...
    //  [0] = resolution
    //  [1] = product name
    product = formatProduct(product);

    //new WebTiledLayer getting created programatically
    var modis = new esri.layers.WebTiledLayer("http://${subDomain}.vis.earthdata.nasa.gov/wmts-geo/wmts.cgi?TIME=" + selectedDate + "&SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=" + product[1] + "&STYLE=&TILEMATRIXSET=EPSG4326_" + product[0] + "&TILEMATRIX=${level}&TILEROW=${row}&TILECOL=${col}&FORMAT=image%2Fjpeg", {
      //these are the sub domains associated with http://vis.earthdata.nasa.gov
      "subDomains": nasaSubDomains, 
      "copyright": "NASA",
      "id": "modis-baselayer",
      "tileInfo": tileInfo
    });
    map.addLayer(modis);

    //moving the layer to the bottom
    map.reorderLayer(modis,0);    
  }
}

function changeOverlay(product) {
  if(map) {
    if(product == "") { //user is turning off overlay
      map.getLayer("modis-overlay").hide();
      return; //FUNCTION ENDS HERE.
    }

    //user has selected another overlay
    //remove existing overlay
    var lyr = map.getLayer("modis-overlay");
    map.removeLayer(lyr);

    //the data in the listbox (of all the baselayers) contains a value field. 
    //the value is a combination of the resolution and the product name, separated by a "|")
    //formatProduct returns an array...
    //  [0] = resolution
    //  [1] = product name
    product = formatProduct(product);

    //new WebTiledLayer getting created programatically
    var modis = new esri.layers.WebTiledLayer("http://${subDomain}.vis.earthdata.nasa.gov/wmts-geo/wmts.cgi?TIME=" + selectedDate + "&SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=" + product[1] + "&STYLE=&TILEMATRIXSET=EPSG4326_" + product[0] + "&TILEMATRIX=${level}&TILEROW=${row}&TILECOL=${col}&FORMAT=image%2Fpng", {
      "subDomains": nasaSubDomains, 
      "copyright": "NASA",
      "id": "modis-overlay",
      "tileInfo": tileInfo
    });
    map.addLayer(modis);

    //moving the layer to the bottom    
    map.reorderLayer(modis,1);
  }
}


//formatDate returns a date that has adds a "0" in the month and day if < 10
function formatDate(D) {
    var m = D.getMonth() + 1; //getMonth returns a 0 = January
    var d = D.getDate();

    m = (m<10) ? m = "0" + m.toString(): m.toString();
    d = (d<10) ? d = "0" + d.toString(): d.toString();

    return D.getFullYear() + "-" + m + "-" + d; //returns YYYY-MM-DD
}

function formatProduct(product) {
  //the data in the listbox (of all the baselayers) contains a value field. 
  //the value is a combination of the resolution and the product name, separated by a "|")
  //formatProduct returns an array...
  //  [0] = resolution
  //  [1] = product name  
  return product.split("|");
}

$(document).ready(function () {
    var theme = "fresh";
    
    //Splitter is used for the left and right panel.
    $('#splitter').jqxSplitter({ 
      width: '100%', 
      height: '95%', 
      theme: theme, 
      panels: [{ size: '375px', max: 1000, min: 50 }, { size: '80%', max: 2000 }] 
    });
    //Splitter events...
    $('#splitter').on('resize', function (event) {
        resizeMap();
    });
    $('#splitter').on('expanded', function (event) {
        resizeMap();
    });
    $('#splitter').on('collapsed', function (event) {
        resizeMap();
    });            

    //Calendar widget
    $("#calendar").jqxCalendar({width: '100%', height: 250, theme: theme });
    
    //min date set to 5/8/2012... that's the extent of the historical NASA data.
    $("#calendar").jqxCalendar('setMinDate', new Date(2012, 4, 8));
    $("#calendar").jqxCalendar('setMaxDate', new Date());

    //calendar events
    $('#calendar').on('change', function (event) {
      //reformat date to have MM and DD
      selectedDate = formatDate(event.args.date);

      //execute changeBaseLayer with new selected Date.
      if ($("#listbox-basemap").jqxListBox('selectedIndex') > -1)
        changeBaseLayer($("#listbox-basemap").jqxListBox('getSelectedItem').value);

      //execute changeOverlay with new selected Date.
      if ($("#listbox-overlay").jqxListBox('selectedIndex') > -1)
        changeOverlay($("#listbox-overlay").jqxListBox('getSelectedItem').value);
    });

    //import CSV from scripts/modis.csv. Contains a CSV of the different NASA products
    var basemapCSV =
      {
          datatype: "csv",
          datafields: [
              { name: 'Instrument' },
              { name: 'Product' },
              { name: 'Layer' },
              { name: 'Format' }
          ],
          url: 'scripts/modis.csv'
      };
      var dataAdapter = new $.jqx.dataAdapter(basemapCSV);

    //populate the baselayer listbox in the left-panel
    $("#listbox-basemap").jqxListBox({ 
      selectedIndex: 1,
      source: dataAdapter, 
      displayMember: "Product", 
      valueMember: "Layer", 
      width: '100%', 
      height: '290px', 
      theme: theme 
    });

    //import CSV from scripts/modis-overlay.csv. Contains a CSV of the different NASA overlay products
    var overlayCSV =
      {
          datatype: "csv",
          datafields: [
              { name: 'Instrument' },
              { name: 'Product' },
              { name: 'Layer' },
              { name: 'Format' }
          ],
          url: 'scripts/modis-overlay.csv'
      };
      dataAdapter = new $.jqx.dataAdapter(overlayCSV);

    //populate the overlay listbox in the left-panel
    $("#listbox-overlay").jqxListBox({ 
      selectedIndex: 0,      
      source: dataAdapter, 
      displayMember: "Product", 
      valueMember: "Layer", 
      width: '100%', 
      height: '346', 
      theme: theme 
    });

    //events for basemap listbox
    $("#listbox-basemap").on('select', function (event) {
        if (event.args) {
            var item = event.args.item;
            if(item) {
              //on selected execute changeBaseLayer to get new tiles              
              changeBaseLayer(item.value);
            }
        }
    });

    $("#listbox-overlay").on('select', function (event) {
        if (event.args) {
            var item = event.args.item;
            if(item) {
              //on selected execute changeOverlay to get new tiles
              changeOverlay(item.value);
            }
        }
    });    
});

//run init() once dojo has been loaded.
dojo.ready(init);

